set(YamlParseTest_SourceFiles
		"YamlParseTest.cpp"
	)

add_executable(YamlParseTest ${YamlParseTest_SourceFiles})
target_include_directories(YamlParseTest PUBLIC "${PROJECT_SOURCE_DIR}/include")
target_include_directories(YamlParseTest PUBLIC  ${CATCH_INCLUDE})
target_link_libraries(YamlParseTest FarmGameBase)
target_link_libraries(YamlParseTest ${YAMLCPP_LIBRARIES})

install(FILES ${Assets} DESTINATION catch)
	
add_test(NAME YamlParseTest COMMAND YamlParseTest)

	