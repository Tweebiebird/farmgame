#include "YamlParse.h"
#include "Helper.h"
#include "exception"

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

TEST_CASE( "YAML Parsing successful", "[yaml]" ) {
    yamlparse::YamlParse parse;
    REQUIRE(parse.openFile());

    SECTION("Parsing Ingredients") {
        std::vector<yamlparse::Ingredient> ingredients = parse.getIngredients();
        REQUIRE(ingredients.size() == 16);
    }

    SECTION("Parsing Machines") {
        try {
            parse.parseMachines();
            {
                yamlparse::Machine& machine = parse.getMachine("wheatfarm");
                REQUIRE(machine.getSize() == 1);
                //Try wheatfarm recipe. No input should output some wheat
                std::vector<int> input;
                std::vector<int> output = machine.getOutput(input);
                REQUIRE(output.size() == 1);
                REQUIRE(output[0] == machine.getIngredientInt("wheat"));
            }
            //Try assembler blt recipe
            {
                yamlparse::Machine& machine = parse.getMachine("assembler");
                std::vector<int> input{ machine.getIngredientInt("slicedbread") , machine.getIngredientInt("tomato") , machine.getIngredientInt("lettuce") , machine.getIngredientInt("cookedbacon") };
                std::vector<int> output = machine.getOutput(input);
                REQUIRE(output[0] == machine.getIngredientInt("blt"));
            }
        }
        catch(yamlparse::InvalidYamlFile e){
            INFO(e.errorMessage);
            REQUIRE(false);
        }
    }
}

TEST_CASE("Yaml Parsing Failures") {
    //TODO. Allow me to specify what file I want to load in  REQUIRETHROWS
}

TEST_CASE("Helper Functions") {
    std::string toSplit = "Hello, This, Is, Delim";
    std::vector<std::string> splitArray = (splitDelim(toSplit, ", "));
    REQUIRE(splitArray.size() == 4);
    REQUIRE(splitArray[3] == "Delim");
    REQUIRE(splitArray[1] == "This");
}

TEST_CASE("Testing Gameboard setup/Tick functions") {
/*    farmgame::GameBoard gameboard();
    gameboard.addMachine("wheatFarm", 0,0);
    gameboard.addConveyor(0, -1, farmgame::direction::UP, farmgame::direction::DOWN);
    gameboard.tick();
    gameboard.getAt(0, 1).out == gameboard.getAt(0, 1).getIngredientInt("wheat");
    */
}
