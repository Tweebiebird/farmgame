set(SDL2_IMAGE_INCLUDE_DIRS "${CMAKE_CURRENT_LIST_DIR}/include")

# Support both 32 and 64 bit builds
if (${CMAKE_SIZEOF_VOID_P} MATCHES 8)
  set(SDL2_IMAGE_LIBRARIES "${CMAKE_CURRENT_LIST_DIR}/lib/x64/SDL2_image.lib")
  set(SDL2_IMAGE_DLL 
      "${CMAKE_CURRENT_LIST_DIR}/lib/x64/SDL2_image.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x64/libjpeg-9.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x64/libpng16-16.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x64/libtiff-5.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x64/libwebp-7.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x64/zlib1.dll"
	  )
else ()
  set(SDL2_IMAGE_LIBRARIES "${CMAKE_CURRENT_LIST_DIR}/lib/x86/SDL2_image.lib")
  set(SDL2_IMAGE_DLL 
      "${CMAKE_CURRENT_LIST_DIR}/lib/x86/SDL2_image.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x86/libjpeg-9.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x86/libpng16-16.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x86/libtiff-5.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x86/libwebp-7.dll"
	  "${CMAKE_CURRENT_LIST_DIR}/lib/x86/zlib1.dll"
	  )
endif ()

string(STRIP "${SDL2_IMAGE_LIBRARIES}" SDL2_IMAGE_LIBRARIES)
string(STRIP "${SDL2_IMAGE_DLL}" SDL2_IMAGE_DLL)