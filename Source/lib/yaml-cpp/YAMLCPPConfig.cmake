set(YAMLCPP_INCLUDE_DIRS "${CMAKE_CURRENT_LIST_DIR}/include")

if (${CMAKE_SIZEOF_VOID_P} MATCHES 8)
	set(YAMLCPP_LIBRARIES "${CMAKE_CURRENT_LIST_DIR}/lib/x64/yaml-cppd.lib")
else ()
	set(YAMLCPP_LIBRARIES "${CMAKE_CURRENT_LIST_DIR}/lib/x86/yaml-cppd.lib")
endif ()

string(STRIP "${YAMLCPP_LIBRARIES}" YAMLCPP_LIBRARIES)