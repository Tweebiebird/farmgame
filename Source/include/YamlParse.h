#include <vector>
#include <map>
#include <optional>
#include <memory>

namespace yamlparse {

//forward declarations
class Machine;

using Ingredient = std::pair<int, std::string>;

//Class that opens the yaml file and parses it
class YamlParse {
public:
    bool openFile();
    //return refernce to variable in namespace scope
    std::vector<Ingredient>& getIngredients();

    //Reads in machines from Yaml. If getIngredients hasn't been called it will call it
    bool parseMachines();
    //Gets machine by name from parsed machines. If parseMachines hasn't been called it will call it
    std::reference_wrapper<Machine> getMachine(std::string machineName);
};

//Holds the recipes. Parsed from yaml strings.
struct Recipe {
    std::vector<int> in;
    std::vector<int> out;
    Recipe(std::vector<int> in, std::vector<int> out) :in(in), out(out) {};
    //Checks if the recipes inputs are equal
    bool operator== (const std::vector<int> &input)const;
};

//Machine. This stores the recipe for the machine and handles converting inputs into the outputs based on them
class Machine {
    //Name of machine
    const std::string name;
    //Size of machine in blocks^2
    const int size;
    //Holds the recipes for the machine
    std::map<int, std::unique_ptr<Recipe>> recipes;
    //Whether the machine has any inputs or just makes things
    bool hasNoInput = false;
    //Reads in the recipe and returns the object it creates
    std::unique_ptr<Recipe> parseRecipe(std::string recipeString, std::vector<Ingredient>& ingredients);
public:
    Machine(std::string name, int size, std::vector<std::string>& recipeStrings, std::vector<Ingredient>& ingredients) :size(size), name(name) {
        for (size_t i = 0; i < recipeStrings.size(); i++) {
            recipes[i]=std::move(parseRecipe(recipeStrings[i], ingredients));
        }
    };
    ~Machine() {};
    const std::string getName() {return name;}
    const int getSize() {return size;}
    //TODO. Change getingredientint to be outside of machine class as it is not really related to individual Machine objects
    //Gets the int key connected to the ingredient string
    int getIngredientInt(std::string ingredientStrs);
    //Finds what the output should be given a certain input. Searches through machine recipes
    const std::vector<int> getOutput(const std::vector<int>& input);
};




//Exceptions
struct InvalidYamlFile {
    std::string function;
    int line;
    std::string errorMessage;
    InvalidYamlFile(std::string function, int line, std::string errorMessage) :function(function), line(line), errorMessage(errorMessage) {};
};
}