#include "YamlParse.h"
#include <unordered_map>
#include <array>

namespace gameboard
{
    enum class Type {None, Machine, MachineOut, Conveyor};
    enum class Direction {Up, Right, Down, Left};

    // Holds the 4x4 chunks of map cells and their contents
    class Chunk
    {
        // What machine is in the cell
        std::array<Type, 16> types;
        // What ingredient is in the cell currently
        std::array<int, 16> contents;
    };
    // Holds all of the chunks. Use this class to get cells by coordinate.
    // The map mainly just holds where all the ingredients are. Conveyors and machines are handled by
    // different data.
    class Map
    {
        std::vector<Chunk> chunks;
        std::vector<std::pair<int, int>> relativePosition;

        int findChunkinMap(std::pair<int,int> coords);
        int findCellinChunk(int chunckIndex, std::pair<int, int> coords);
    public:
        // Returns <chunk index in map, cell index in chunk>
        std::pair<int, int> getCellIndicies(std::pair<int, int> coords);
    };

    // Holds data about where the conveyors are.
    class Conveyors
    {
        std::vector<std::pair<int, int>> coord;
        std::vector<Direction> directions;
    };

    struct OutCoords
    {
        // Location of the out
        std::vector<std::pair<int, int>> coord;
        // Which recipe output we are outputing. Normally it is just 1.
        // If the recipe has multiple outputs this syas which is outed where
        std::vector<short> thing;
        // Which direction it pushes it out
        std::vector<Direction> direction;
    };

    // Holds data about where all the machines are. 
    class Machines
    {
        // Always Square
        std::vector<int> size;
        // Coordinates of the bottom left square
        std::vector<std::pair<int, int>> blCoords;
        // Where the outs are
        std::vector<OutCoords> outCoords;
    };

    // Moves ingredients between map cells based on conveyor data
    void calcConveyors(Conveyors const & conveyors, Map& map);
    // Removes ingredients from map cells and adds new ones based on machine data
    void calcMachines(Machines const & machines, Map& map);
}