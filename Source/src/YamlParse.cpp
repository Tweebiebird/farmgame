#include "YamlParse.h"

#include "Helper.h"

#include "yaml-cpp/yaml.h"

#include <memory>
#include <algorithm>
namespace yamlparse
{
    YAML::Node config;

    std::vector<Ingredient> ingredients;

    std::vector<std::unique_ptr<Machine>> machines;

    bool YamlParse::openFile()
    {
        config = YAML::LoadFile("recipes.yml");

        return !config.IsNull();
    }

    std::vector<Ingredient>& YamlParse::getIngredients() {
        std::vector<std::string> ingredientStrings = config["ingredients"].as<std::vector<std::string>>();
        for (size_t i = 0; i < ingredientStrings.size();i++) {
            ingredients.push_back(Ingredient(i, ingredientStrings[i]));
        }
        return ingredients;
    }

    //Reads in all the machines in from the YAML file and stores them in free namespace variable
    bool YamlParse::parseMachines()
    {
        //if no ingredients yet try parse them
        if (ingredients.size() == 0) {
            getIngredients();
        }
        //Thanks https://stackoverflow.com/questions/31368135/yaml-cpp-read-sequence-in-item
        //Get all the machines from YAML
        YAML::Node& machinesYaml = config["machines"];
        if (machinesYaml.size() == 0) {
            return false;
        }
        //Iterate through them
        for (YAML::iterator machineIt = machinesYaml.begin(); machineIt != machinesYaml.end(); ++machineIt) {
            //Read in Size
            YAML::Node& machineYaml = *machineIt;
            int machineSize = machineYaml["size"].as<int>();
            std::vector<std::string> recipeStrings = machineYaml["recipes"].as<std::vector<std::string>>();
            std::string name = machineYaml["name"].as<std::string>();
            machines.push_back(std::make_unique<Machine>(name, machineSize, recipeStrings, ingredients));
        }
        return true;
    }

    std::reference_wrapper<Machine> YamlParse::getMachine(std::string machineName)
    {
        //If we have not parsed machines try parse them
        if (machines.size() == 0) {
            parseMachines();
        }
        for (auto&& machine : machines) {
            if (machine->getName() == machineName) {
                return std::ref(*machine);
            }
        }
        throw InvalidYamlFile(__FUNCTION__, __LINE__, "No machine " + machineName + " found in parsed machines");
    }

    std::unique_ptr<Recipe> Machine::parseRecipe(std::string recipeString, std::vector<Ingredient>& ingredients)
    {
        std::vector<std::string> inout= splitDelim(recipeString, "=");
        if (inout.size() > 2) {
            throw InvalidYamlFile(__FUNCTION__, __LINE__, "Too many = in recipe. You can only have 1");
        }
        std::vector<std::string> inStr;
        //If your first string(your input) is empty then that means you have a producer machine
        if (inout[0].size() == 0) {
            hasNoInput = true;
        }
        else {
            inStr = splitDelim(inout[0], "+");
        }
        std::vector<std::string> outStr = splitDelim(inout[1], "+");

        std::vector<int> in;
        for (std::string ingredientStr : inStr) {
            in.push_back(getIngredientInt(ingredientStr));
        }
        std::vector<int> out;
        for (std::string ingredientStr : outStr) {
            out.push_back(getIngredientInt(ingredientStr));
        }
        if (in.size() + out.size() > (size_t)size * 4) {
            throw InvalidYamlFile(__FUNCTION__, __LINE__, "Your machine size is too small to support this recipe. You need " + std::to_string(in.size() + out.size()) + "but only have " + std::to_string(size * 4));
        }
        return std::make_unique<Recipe>(in, out);
    }

    int Machine::getIngredientInt(std::string ingredientStr)
    {
        for (Ingredient ingredient:ingredients) {
            if (ingredient.second == ingredientStr) {
                return ingredient.first;
            }
        }
        throw InvalidYamlFile(__FUNCTION__, __LINE__, "You have included ingredient " + ingredientStr + " which is not present in your ingredient list");
    }

    const std::vector<int> Machine::getOutput(const std::vector<int>& input)
    {
        for (auto&& recipePair: recipes) {
            if (*recipePair.second.get() == input) {
                return recipePair.second.get()->out;
            }
        }
        return std::vector<int>();
    }

    //Baiscally just sorts and the compares the 2 arrays
    bool Recipe::operator==(const std::vector<int>& inputRhs) const
    {
        //If they are different sizes the we don;t even need to copy them
        if (inputRhs.size() != in.size()) {
            return false;
        }

        //Make copies. We don't want to modify them
        std::vector<int> inputCopy(in.size());
        std::copy(in.begin(), in.end(), inputCopy.begin());
        std::vector<int> inputRhsCopy(inputRhs.size());
        std::copy(inputRhs.begin(), inputRhs.end(), inputRhsCopy.begin());
        std::sort(inputCopy.begin(), inputCopy.end());
        std::sort(inputRhsCopy.begin(), inputRhsCopy.end());
        for (size_t i = 0; i < inputCopy.size(); i++) {
            if (inputCopy[i] != inputRhsCopy[i]) {
                return false;
            }
        }
        return true;
        
    }

}
