#include "Helper.h"

std::vector<std::string> splitDelim(std::string toSplit, std::string delim)
{
    std::vector<std::string> splitArray;
    bool loop = true;
    while (loop) {
        size_t pos = toSplit.find(delim);
        if (pos != std::string::npos) {

            //We need to get the substr from the start to the position of the delim
            splitArray.push_back(toSplit.substr(0, pos));
            //ON the other side of the delim we want to go from where the delim ends. So we need to include the delim size
            toSplit = toSplit.substr(pos + delim.size(), toSplit.size() - pos);
        }
        else {
            splitArray.push_back(toSplit);
            loop = false;
        }
    }
    return splitArray;
}
