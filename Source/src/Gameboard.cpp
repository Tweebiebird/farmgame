#include "..\include\Gameboard.h"

int gameboard::Map::findChunkinMap(std::pair<int, int> coords)
{
    return 0;
}

int gameboard::Map::findCellinChunk(int chunckIndex, std::pair<int, int> coords)
{
    return 0;
}

std::pair<int, int> gameboard::Map::getCellIndicies(std::pair<int, int> coords)
{
    int chunkCoord = findChunkinMap(coords);
    int cellCoord = findCellinChunk(chunkCoord, coords);
    return std::make_pair(chunkCoord, cellCoord);
}

void gameboard::calcConveyors(Conveyors const & conveyors, Map & map)
{
}

void gameboard::calcMachines(Machines const & machines, Map & map)
{
}
