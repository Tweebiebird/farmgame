To build

Download nessecary libraries and put them in lib (SDL, SDL_image, catch, yaml-cpp)
SDL links with both a DLL and a import library for boht 64 and 32 bit. You will also need SDL_image
SDL2.dll
SDL2.lib
SDL2_image.dll
SDL2_image.lib
As well as some others. These can be downloaded as a pack from the SDL website. I have written some Cmake.config files to help with linking

For yaml-cpp the libs are not provided. You will have to build you own static libraries in x64 and x32. You can do this with CMake. 
Once they are built copy them into the folders indicated by the CmakeConfig.

Catch is just a header so that is easy